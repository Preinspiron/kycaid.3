import Splide from "@splidejs/splide";

const splide = new Splide(".splide", {
  arrows: true,

  pagination: false,
  gap: 24,
  // omitEnd: true,
  perPage: 4,
  // rewind: true,
  classes: {
    list: "splide__list verification__list",
    pagination: "splide_pagination how-ocr-works--pagination",
    page: "item",
  },

  mediaQuery: "max",
  breakpoints: {
    1028: {
      // fixedWidth: "85%",
      arrows: true,
      // rewind: true,
      perPage: 3,
      pagination: false,
    },
    768: {
      // fixedWidth: "85%",
      arrows: false,
      // rewind: true,
      perPage: 2,
      pagination: true,
    },
    431: {
      fixedWidth: "85%",
      arrows: false,
      // rewind: true,
      perPage: 1,
      pagination: true,
    },
  },
});

splide.mount();
