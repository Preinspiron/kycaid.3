import lottie from "lottie-web";

import animationData1 from "/assets/pages/livenesscheck/lottie-animations/active.json";
import animationData2 from "/assets/pages/livenesscheck/lottie-animations/passive.json";
import animationData3 from "/assets/pages/livenesscheck/lottie-animations/Anti-fraud-mechanism.json";

const containers = document.querySelectorAll(".lottie-animation-product");

const addLottieAmimation = (container, data) => {
  const anim = lottie.loadAnimation({
    container: container,
    renderer: "svg",
    loop: true,
    autoplay: true,
    animationData: data,
  });
};

addLottieAmimation(containers[0], animationData1);
addLottieAmimation(containers[1], animationData2);
addLottieAmimation(containers[2], animationData3);
