// import { Client } from "@hubspot/api-client";

// const hubspotClient = new Client({ accessToken: 'token' });

// console.log(hubspotClient);

// const getContacts = async () => {
//   try {
//     const allContacts = await hubspotClient.crm.contacts;
//     console.log(allContacts);
//   } catch (error) {
//     console.error("Ошибка запроса:", error.response || error.message);
//   }
// };

// getContacts();
            
// This example displays how to get all contacts from a HubID and paginate through them using the 'offset' parameter.
// The end result is an array containing all parsed contacts.

import { Client } from "@hubspot/api-client";
import axios from "axios";

const hubspotClient = new Client({ accessToken: 'pat-eu1-fb5b3f7d-664e-42d6-812d-49346158a055' });



const getContacts = async () => {
  try {
    const response = await hubspotClient.apiRequest({
      path: '/crm/v3/objects/contacts',
    })
    const json = await response.json()
    console.log(json)
  } catch (error) {
    console.error("Ошибка запроса:", error.response || error.message);
  }
};

getContacts();

const elements = {
  openModalContact: document.querySelector("[data-modal-contact-open]"),
  closeModalBtn: document.querySelector("[data-modal-close]"),
  submitBtn: document.querySelector("[data-modal-submit]"),
  modalContacts: document.querySelector("[data-modal-contacts]"),
  modalSuccess: document.querySelector("[data-modal-success]"),
  backDrop: document.querySelector("[data-backdrop]"),
};

const removeListener = () => {
  document.removeEventListener("keydown", closeModal);
  elements.backDrop.removeEventListener("click", closeModal);
  elements.closeModalBtn.removeEventListener("click", closeModal);
  elements.submitBtn.removeEventListener("click", openModalSuccess);
};

const closeModal = (event) => {
  if (
    event.code === "Escape" ||
    event.target === elements.backDrop ||
    event.currentTarget === elements.closeModalBtn
  ) {
    elements.modalContacts.classList.toggle("is-hidden");
    elements.backDrop.classList.toggle("is-hidden");
    removeListener();
  }
};

const closeModalSuccess = (event) => {
  if (event.code === "Escape" || event.target === elements.backDrop) {
    elements.modalSuccess.classList.toggle("is-hidden-success");
    elements.backDrop.classList.toggle("is-hidden");

    document.removeEventListener("keydown", closeModalSuccess);
    elements.backDrop.removeEventListener("click", closeModalSuccess);
  }
};

const openModal = () => {
  elements.modalContacts.classList.toggle("is-hidden");
  elements.backDrop.classList.toggle("is-hidden");

  document.addEventListener("keydown", closeModal);
  elements.backDrop.addEventListener("click", closeModal);
  elements.closeModalBtn.addEventListener("click", closeModal);
  elements.submitBtn.addEventListener("click", handleSubmit);
};

const openModalSuccess = () => {
  elements.modalSuccess.classList.toggle("is-hidden-success");
  elements.modalContacts.classList.toggle("is-hidden");

  removeListener();

  document.addEventListener("keydown", closeModalSuccess);
  elements.backDrop.addEventListener("click", closeModalSuccess);
};

const handleSubmit = async (event) => {
  event.preventDefault();
  //  await getContacts();
  openModalSuccess();
};

elements.openModalContact.addEventListener("click", openModal);
