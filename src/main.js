import "./style.scss";
import "./js/header/header.js";
import "./js/gsap.js";
import "./js/splide.js";
import "./js/hero/heroAnimation.js";
import "./js/government-checks/lottieAnimation.js";
import "./js/modal/modal.js";
import "./js/zoomSections.js";
