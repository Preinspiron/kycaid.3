import "./livenesscheck.scss";
import "../../js/livenesscheck/zoomSections.js";
import "../../js/header/header.js";
import "../../js/livenesscheck/products-features/lottie-animation-products.js";
import "../../js/livenesscheck/products-features/products-features.js";
import "../../js/livenesscheck/faq/faq.js";