import { defineConfig } from "vite";
import injectHTML from "vite-plugin-html-inject";
import webfontDownload from "vite-plugin-webfont-dl";
import ViteImagemin from "vite-plugin-imagemin";
import beautify from "vite-plugin-beautify";
import { resolve } from "path";

export default defineConfig({
  root: "src",

  build: {
    outDir: "../dist",
    emptyOutDir: true,
    rollupOptions: {
      input: {
        index: resolve(__dirname, "src/index.html"),
        livenesscheck: resolve(__dirname, "src/pages/livenesscheck/index.html"),
        id_verifications: resolve(
          __dirname,
          "src/pages/id_verifications/index.html"
        ),
        kyb: resolve(__dirname, "src/pages/kyb/index.html"),
        ocr: resolve(__dirname, "src/pages/ocr/index.html"),
        kyc_verification: resolve(__dirname, "src/pages/kyc-verification/index.html"),
      },
      output: {
        chunkFileNames: "assets/js/[name]-[hash].js",
        entryFileNames: "assets/js/[name]-[hash].js",

        assetFileNames: (data) => {
          if (/\.(gif|jpe?g|png|svg|webp)$/.test(data.name ?? "")) {
            return "assets/img/[name]-[hash][extname]";
          }

          if (/\.css$/.test(data.name ?? "")) {
            return "assets/css/[name]-[hash][extname]";
          }

          if (/\.woff|woff2|ttf|otf|eot$/.test(data.name ?? "")) {
            return "assets/fonts/[name]-[hash][extname]";
          }

          return "assets/[name]-[hash][extname]";
        },
      },
    },
  },
  css: {
    devSourcemap: true,
  },
  plugins: [
    ViteImagemin({
      gifsicle: {
        optimizationLevel: 8,
        interlaced: false,
      },
      optipng: {
        optimizationLevel: 8,
      },
      mozjpeg: {
        quality: 80,
      },
      pngquant: {
        quality: [0.8, 0.9],
        speed: 4,
      },
      svgo: {
        plugins: [
          {
            name: "removeViewBox",
          },
          {
            name: "removeEmptyAttrs",
            active: false,
          },
        ],
      },
    }),
    injectHTML(),
    beautify({
      inDir: "dist",
      css: {
        enabled: false,
      },
      js: {
        enabled: false,
      },
      html: {
        options: {
          end_with_newline: true,
          indent_size: 4,
          indent_with_tabs: true,
          indent_inner_html: true,
          preserve_newlines: true,
          extra_liners: ["head", "body", "/html", "header", "main", "footer"],
          inline: [
            "abbr",
            "area",
            "b",
            "bdi",
            "bdo",
            "br",
            "cite",
            "code",
            "data",
            "datalist",
            "del",
            "dfn",
            "em",
            "embed",
            "ins",
            "kbd",
            "keygen",
            "label",
            "map",
            "mark",
            "math",
            "meter",
            "object",
            "output",
            "progress",
            "q",
            "ruby",
            "s",
            "samp",
            "small",
            "strong",
            "sub",
            "sup",
            "template",
            "u",
            "var",
            "wbr",
            "text",
            "acronym",
            "address",
            "big",
            "dt",
            "ins",
            "strike",
            "tt",
          ],
        },
      },
    }),
    webfontDownload([
      "https://fonts.googleapis.com/css2?family=Anek+Bangla:wght@400;600;700&family=DM+Sans:opsz,wght@9..40,400;9..40,700&display=swap",
    ]),
  ],
});
